# mirror_networking

[Unity - Mirror Networking Tutorial](https://www.youtube.com/watch?v=oR9P0gSaAOU&list=PLkx8oFug638oBYF5EOwsSS-gOVBXj1dkP&index=2)

## Setup

* Import Mirror from the Asset Store.
*** IMPORTANT --  You must restart Unity after importing Mirror for the Components Menu to update! ***.

* Set Scripting Runtime Version to '.NET 4.x' (in Build Settings > Player Settings > Other Settings)
